#!/bin/bash
set -eo pipefail
echo "Subiendo Mis imagenes a Gitlab"
filename="version"
while read -r line; do
    echo "$line" 
    docker tag server registry.gitlab.com/kcdgt/cluster-management:server${line}    
    docker push registry.gitlab.com/kcdgt/cluster-management:server${line}  
    echo "FIN DEL SCRIPT"
done < "$filename"